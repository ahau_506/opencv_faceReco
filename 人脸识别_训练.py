import numpy as np
from PIL import Image # python里面的图像库
import os
import cv2

# Path for face image database
path = './userImg' # 数据集的路径


# 在OpenCV中，可以用函数cv2.face.LBPHFaceRecognizer_create()生成LBPH识别器实例模型，然后应用cv2.face_FaceRecognizer.train()函数完成训练，最后用cv2.face_FaceRecognizer.predict()函数完成人脸识别。
# https://blog.csdn.net/qq_43069920/article/details/103673125

# LBPH识别器实例模型
recognizer = cv2.face.LBPHFaceRecognizer_create()

# 获取本地安装的cv2目录下的人脸检测级联分类器
detector = cv2.CascadeClassifier(r'C:\Users\hotpotman\.conda\envs\face\Lib\site-packages\cv2\data\haarcascade_frontalface_default.xml');

# 定义一个函数 用来获取 LBPH识别器 训练的图像和标签
def getImagesAndLabels(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
    # print(imagePaths) # 输出文件夹所有的文件路径
    faceSamples=[]  # 存放人脸
    ids = []       # 存放人脸的ID
    for imagePath in imagePaths:  # 遍历路径
        PIL_img = Image.open(imagePath).convert('L') # convert it to grayscale
        img_numpy = np.array(PIL_img,'uint8')
        print(os.path.split(imagePath)[-1])
        # 获取图片中包含的人脸id
        id = int(os.path.split(imagePath)[-1].split(".")[0]) # 获取ID
        print(id)
        #print(" " + str(id)) # 输入ID
        faces = detector.detectMultiScale(img_numpy)
        for (x,y,w,h) in faces:
            faceSamples.append(img_numpy[y:y+h,x:x+w])
            ids.append(id)
    return faceSamples,ids

print ("\n [INFO] Training faces. It will take a few seconds. Wait ...")
faces,ids = getImagesAndLabels(path)
# 传入图像和标签 ，训练 LBPH识别器
recognizer.train(faces, np.array(ids))

# 将训练好的识别器 导出为 .yml 文件
recognizer.write('./trainer/trainer.yml') # recognizer.save() worked on Mac, but not on Pi

# Print the numer of faces trained and end program
print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(ids))))
