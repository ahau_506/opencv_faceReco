import cv2
import os

cam = cv2.VideoCapture(0)  # 获取摄像头
cam.set(3, 640) # set video width  # 设置视频的高度和宽度
cam.set(4, 480) # set video height

# 获取本地安装的cv2目录下的级联分类器
face_detector = cv2.CascadeClassifier(r'C:\Users\hotpotman\.conda\envs\face\Lib\site-packages\cv2\data\haarcascade_frontalface_default.xml')

# 对于每个人，输入一个编号用以区分
face_id = input('\n enter user id end press <return> ==>  ')

#print("\n [INFO] Initializing face capture. Look the camera and wait ...")
# Initialize individual sampling face count
count = 0

while(True):
    ret, img = cam.read() # 从摄像头读取图像
    # 从文件中读取图像
    # img = cv2.imread("/home/pi/Desktop/uerImg/" + str(count) + '.jpg')
    #print("/home/pi/Desktop/兔子牙/" + str(count) + '.jpg')
    # 翻转图像 1  水平翻转  0 垂直翻转   -1 水平垂直翻转
    #img = cv2.flip(img, -1) # flip video image vertically
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # 转化为灰度图
    # 使用级联分类器进行图片人脸检测
    faces = face_detector.detectMultiScale(gray, 1.2, 5)
    
    count += 1
# 在人脸上画矩形
    for (x,y,w,h) in faces:
        cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0), 2)
        

        # 将图片保存到 userImg目录下
        cv2.imwrite("./userImg/" + str(face_id) + '.' + str(count) + ".jpg", gray[y:y+h,x:x+w])
        cv2.imshow('image', img) # 显示
        
    cv2.imshow('image', img) # 显示
    # 当按下 ESC 或者采集到 10 张图片时 退出
    k = cv2.waitKey(100) & 0xff 
    if k == 27:
        break
    elif count >= 10: 
        break

# 关闭所有窗口，并释放资源
print("\n [INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()
